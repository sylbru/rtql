module Tests exposing (all)

import Element as El
import Html.Attributes as Attr
import Json.Encode as Enc
import Main as App
import ProgramTest exposing (ProgramTest, clickButton, expectViewHas, expectViewHasNot, fillIn)
import Test exposing (Test, describe, test)
import Test.Html.Selector exposing (attribute, text)


start : Enc.Value -> ProgramTest App.Model App.Msg (Cmd App.Msg)
start flags =
    ProgramTest.createElement
        { init = App.init
        , update = App.update
        , view = App.view >> El.layout []
        }
        |> ProgramTest.start flags


all : Test
all =
    describe "Acceptance tests"
        [ test "Default phrase" <|
            \_ ->
                start Enc.null
                    |> expectViewHas [ text "Il reste treize fraises fraîches" ]
        , test "Saved phrase" <|
            \_ ->
                start (Enc.list Enc.string [ "session irlandaise" ])
                    |> expectViewHas [ text "session irlandaise" ]
        , test "Import replace sets phrases" <|
            \_ ->
                start (Enc.list Enc.string [ "problème" ])
                    |> clickButton "Éditer les phrases"
                    |> clickButton "Importer"
                    |> fillIn App.importId App.importLabel "sérieusement"
                    |> clickButton "Ajouter les phrases"
                    |> clickButton "Éditer les phrases"
                    |> expectViewHas [ attribute (Attr.value "problème") ]
        , test "Import append adds phrases" <|
            \_ ->
                start (Enc.list Enc.string [ "problème" ])
                    |> clickButton "Éditer les phrases"
                    |> clickButton "Importer"
                    |> fillIn App.importId App.importLabel "sérieusement"
                    |> clickButton "Remplacer les phrases"
                    |> clickButton "Éditer les phrases"
                    |> expectViewHasNot [ attribute (Attr.value "problème") ]
        ]
