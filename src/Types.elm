module Types exposing (BackendModel, BackendMsg(..), FrontendModel, FrontendMsg(..), ToBackend(..), ToFrontend(..))

import Animator
import Browser exposing (UrlRequest)
import Browser.Navigation exposing (Key)
import Dict exposing (Dict)
import Http
import Phrase exposing (PhraseId)
import Phrasebook exposing (Phrasebook, PhrasebookId)
import Random
import RemoteData exposing (RemoteData)
import Route exposing (Route)
import Time
import Url exposing (Url)


type alias FrontendModel =
    { key : Key
    , phrasebook : Maybe ( PhrasebookId, RemoteData Http.Error Phrasebook )
    , currentPhrase : Int
    , previousPhrase : Maybe Int
    , ticking : Bool

    -- Necessary to have a fluid animation by setting currentPhrase and previousPhrase
    -- at the same tick when the animation is starting, instead of when the data is received
    -- and the animation is scheduled ("immediately" but actually after a frame)
    , animationToRunAtNextTick : Maybe { current : Int, previous : Int }
    , animationDone : Animator.Timeline Bool
    , importText : String
    , importReplaceNeedsConfirm : Bool
    , route : Route
    }


type alias BackendModel =
    { phrasebooks : Dict PhrasebookId Phrasebook
    , seed : Random.Seed
    }


type FrontendMsg
    = UrlClicked UrlRequest
    | UrlChanged Url
    | ClickedCreatePhrasebook
    | AskedPhrase
    | GotPhrase Int
    | TypedPhrase Int String
    | ClickedAddPhrase
    | ClickedRemovePhrase Int
    | HitArrowKeyUpFrom Int
    | HitArrowKeyDownFrom Int
    | TypedImportText String
    | ClickedImportAppend
    | ClickedImportReplace
    | ConfirmedImportReplace
    | CancelledImportReplace
    | ClickedCopyId
    | ClickedCopyAddress
    | ClickedCopyPhrases
    | BlurredPhraseEditorInput PhraseId
    | FocusedPhraseEditorInput PhraseId
    | Tick Time.Posix
    | NoOp


type ToBackend
    = LoadPhrasebook PhrasebookId
    | SavePhrasebook PhrasebookId Phrasebook
    | CreatePhrasebook


type BackendMsg
    = NoOpBackendMsg


type ToFrontend
    = GotPhrasebook PhrasebookId Phrasebook
    | GotNewPhrasebookId PhrasebookId
    | SavedPhrasebook
