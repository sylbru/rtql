module Phrase exposing (Phrase, PhraseId, empty, isEmpty)


type alias Phrase =
    String


type alias PhraseId =
    Int


empty : Phrase
empty =
    ""


isEmpty : Phrase -> Bool
isEmpty phrase =
    phrase == ""
