module Phrasebook exposing (Phrasebook, PhrasebookId, addPhrases, clean, getPhrase, idGenerator, new, newPhraseId, nextPhraseId, previousPhraseId, randomPhraseGenerator, removePhrase, toString, updatePhrase)

import Phrase exposing (Phrase, PhraseId)
import Random


type alias PhrasebookId =
    String


type alias Phrasebook =
    List ( PhraseId, Phrase )


new : Phrasebook
new =
    []


idGenerator : Random.Generator PhrasebookId
idGenerator =
    let
        letterGenerator : Random.Generator Char
        letterGenerator =
            Random.int 0x61 0x7A
                |> Random.map Char.fromCode
    in
    Random.list 10 letterGenerator
        |> Random.map String.fromList


toString : Phrasebook -> String
toString phrasebook =
    List.map Tuple.second phrasebook
        |> String.join "\n"


getPhrase : PhraseId -> Phrasebook -> String
getPhrase phraseId phrasebook =
    let
        maybePhrase : Maybe ( PhraseId, Phrase )
        maybePhrase =
            phrasebook
                |> List.filter (\( i, _ ) -> i == phraseId)
                |> List.head
    in
    case maybePhrase of
        Just thatPhrase ->
            Tuple.second thatPhrase

        Nothing ->
            Phrase.empty


updatePhrase : PhraseId -> String -> Phrasebook -> Phrasebook
updatePhrase id newValue phrasebook =
    List.map
        (\( id_, oldValue ) ->
            if id_ == id then
                ( id_, newValue )

            else
                ( id_, oldValue )
        )
        phrasebook


removePhrase : PhraseId -> Phrasebook -> Phrasebook
removePhrase id =
    List.filter (\( i, _ ) -> i /= id)


nextPhraseId : PhraseId -> Phrasebook -> Maybe PhraseId
nextPhraseId phraseId phrasebook =
    case phrasebook of
        [] ->
            Nothing

        ( id, _ ) :: [] ->
            Just id

        ( id1, _ ) :: ( id2, phrase2 ) :: rest ->
            if id1 == phraseId then
                Just id2

            else
                nextPhraseId phraseId (( id2, phrase2 ) :: rest)


previousPhraseId : PhraseId -> Phrasebook -> Maybe PhraseId
previousPhraseId phraseId phrasebook =
    phrasebook
        |> List.reverse
        |> nextPhraseId phraseId


randomPhraseGenerator : Phrasebook -> PhraseId -> Random.Generator Int
randomPhraseGenerator phrases currentPhrase =
    let
        phrasesExceptCurrent : List ( PhraseId, Phrase )
        phrasesExceptCurrent =
            case phrases of
                _ :: [] ->
                    phrases

                _ ->
                    List.filter (\( id, _ ) -> id /= currentPhrase) phrases
    in
    case phrasesExceptCurrent of
        first :: rest ->
            Random.uniform first rest
                |> Random.map Tuple.first

        [] ->
            Random.uniform 0 []


clean : Phrasebook -> Phrasebook
clean phrasebook =
    phrasebook
        |> List.map (\( id, phrase ) -> ( id, String.trim phrase ))
        |> List.filter (\( _, phrase ) -> not <| String.isEmpty phrase)
        |> List.indexedMap (\newIndex ( _, phrase ) -> ( newIndex, phrase ))


newPhraseId : Phrasebook -> PhraseId
newPhraseId phrases =
    case phrases of
        [] ->
            1

        head :: tail ->
            let
                maxId : PhraseId
                maxId =
                    List.foldl
                        (\( id, _ ) currentMax ->
                            if id > currentMax then
                                id

                            else
                                currentMax
                        )
                        (Tuple.first head)
                        tail
            in
            maxId + 1


addPhrases : List Phrase -> Phrasebook -> Phrasebook
addPhrases phrases phrasebook =
    case phrases of
        [] ->
            phrasebook

        phrase :: rest ->
            let
                changedPhrasebook : Phrasebook
                changedPhrasebook =
                    ( newPhraseId phrasebook, phrase ) :: phrasebook
            in
            addPhrases rest changedPhrasebook
