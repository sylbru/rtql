module Route exposing (Page(..), Route(..), allCodecs, editorCodec, importCodec, practiceCodec, shareCodec, toAbsoluteUrl)

import Phrasebook exposing (PhrasebookId)
import Url.Codec exposing (Codec)


type Route
    = Landing
    | Phrasebook PhrasebookId Page


type Page
    = Practice
    | Editor
    | Share
    | Import


allCodecs : List (Codec Route)
allCodecs =
    [ practiceCodec, editorCodec, shareCodec, importCodec, landingCodec ]


practiceCodec : Codec Route
practiceCodec =
    Url.Codec.succeed (\id -> Phrasebook id Practice) inPhrasebook
        |> Url.Codec.string getPhrasebookId


editorCodec : Codec Route
editorCodec =
    Url.Codec.succeed (\id -> Phrasebook id Editor) inPhrasebook
        |> Url.Codec.string getPhrasebookId
        |> Url.Codec.s "edit"


shareCodec : Codec Route
shareCodec =
    Url.Codec.succeed (\id -> Phrasebook id Share) inPhrasebook
        |> Url.Codec.string getPhrasebookId
        |> Url.Codec.s "share"


importCodec : Codec Route
importCodec =
    Url.Codec.succeed (\id -> Phrasebook id Import) inPhrasebook
        |> Url.Codec.string getPhrasebookId
        |> Url.Codec.s "import"


landingCodec : Codec Route
landingCodec =
    Url.Codec.succeed Landing (not << inPhrasebook)


inPhrasebook : Route -> Bool
inPhrasebook route =
    case route of
        Landing ->
            False

        Phrasebook _ _ ->
            True


getPhrasebookId : Route -> Maybe String
getPhrasebookId route =
    case route of
        Phrasebook phrasebookId _ ->
            Just phrasebookId

        _ ->
            Nothing


toAbsoluteUrl : List (Url.Codec.Codec Route) -> Route -> String
toAbsoluteUrl codecs route =
    Url.Codec.toString codecs route
        |> Maybe.withDefault "notfound"
        |> (++) "/"
