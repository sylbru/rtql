port module Frontend exposing (app)

import Animator exposing (Animator)
import Animator.Inline
import Browser exposing (UrlRequest(..))
import Browser.Dom
import Browser.Navigation exposing (Key)
import Element as El exposing (Element)
import Element.Background as Background
import Element.Border as Border
import Element.Font as Font
import Element.Input as Input
import Element.Region as Region
import Evergreen.V5.Types exposing (FrontendModel)
import Html
import Html.Attributes as Attr
import Html.Events as Events
import Http
import Json.Decode as Dec
import Lamdera
import Phrase exposing (Phrase, PhraseId)
import Phrasebook exposing (Phrasebook, PhrasebookId)
import Random
import RemoteData exposing (RemoteData(..))
import Route exposing (Page(..))
import Task
import Types exposing (FrontendModel, FrontendMsg(..), ToBackend(..), ToFrontend(..))
import Url exposing (Url)
import Url.Codec


app :
    { init : Url -> Key -> ( FrontendModel, Cmd FrontendMsg )
    , view : FrontendModel -> Browser.Document FrontendMsg
    , update : FrontendMsg -> FrontendModel -> ( FrontendModel, Cmd FrontendMsg )
    , updateFromBackend : ToFrontend -> FrontendModel -> ( FrontendModel, Cmd FrontendMsg )
    , subscriptions : FrontendModel -> Sub FrontendMsg
    , onUrlRequest : UrlRequest -> FrontendMsg
    , onUrlChange : Url -> FrontendMsg
    }
app =
    Lamdera.frontend
        { init = init
        , onUrlRequest = UrlClicked
        , onUrlChange = UrlChanged
        , update = update
        , updateFromBackend = updateFromBackend
        , subscriptions = subscriptions
        , view =
            \model ->
                { title = "RTQl"
                , body =
                    [ El.layout
                        [ Font.family
                            [ Font.typeface "Source Sans Pro"
                            , Font.typeface "Trebuchet MS"
                            , Font.typeface "Lucida Grande"
                            , Font.typeface "Bitstream Vera Sans"
                            , Font.typeface "Helvetica Neue"
                            , Font.sansSerif
                            ]
                        , Font.color ink
                        , Font.center
                        ]
                        (view model)
                    ]
                }
        }


init : Url -> Key -> ( FrontendModel, Cmd FrontendMsg )
init url key =
    case Url.Codec.parseUrl Route.allCodecs url of
        Ok route ->
            let
                emptyModel : FrontendModel
                emptyModel =
                    { phrasebook = Nothing
                    , currentPhrase = 0
                    , previousPhrase = Nothing
                    , animationToRunAtNextTick = Nothing
                    , animationDone = Animator.init True
                    , ticking = False
                    , importText = ""
                    , importReplaceNeedsConfirm = False
                    , route = Route.Landing
                    , key = key
                    }
            in
            emptyModel
                |> gotoRoute route

        Err _ ->
            ( erroredModel (Http.BadUrl (Url.toString url)) key
            , Cmd.none
            )


erroredModel : Http.Error -> Key -> FrontendModel
erroredModel error key =
    { phrasebook = Just ( "", Errored error )
    , currentPhrase = 0
    , previousPhrase = Nothing
    , animationToRunAtNextTick = Nothing
    , animationDone = Animator.init True
    , ticking = False
    , importText = ""
    , importReplaceNeedsConfirm = False
    , route = Route.Landing
    , key = key
    }


animator : Animator.Animator FrontendModel
animator =
    Animator.animator
        |> Animator.watching
            .animationDone
            (\newState model ->
                { model | animationDone = newState }
            )


update : FrontendMsg -> FrontendModel -> ( FrontendModel, Cmd FrontendMsg )
update msg model =
    case msg of
        UrlClicked urlRequest ->
            case urlRequest of
                Internal url ->
                    ( model, Browser.Navigation.pushUrl model.key (Url.toString url) )

                External url ->
                    ( model, Browser.Navigation.load url )

        UrlChanged url ->
            case Url.Codec.parseUrl Route.allCodecs url of
                Ok route ->
                    model
                        |> gotoRoute route

                Err _ ->
                    ( erroredModel (Http.BadUrl (Url.toString url)) model.key, Cmd.none )

        ClickedCreatePhrasebook ->
            ( model, Lamdera.sendToBackend CreatePhrasebook )

        AskedPhrase ->
            ( model, getRandomPhrase model )

        GotPhrase phraseId ->
            ( case model.phrasebook of
                Just ( _, Loaded _ ) ->
                    { model
                        | animationToRunAtNextTick = Just { current = phraseId, previous = model.currentPhrase }
                        , ticking = True
                        , animationDone =
                            model.animationDone
                                |> Animator.interrupt
                                    [ Animator.event Animator.immediately False
                                    , Animator.event Animator.quickly True
                                    ]
                    }

                _ ->
                    model
            , Cmd.none
            )

        TypedPhrase phraseId value ->
            let
                newModel : FrontendModel
                newModel =
                    { model
                        | phrasebook =
                            Maybe.map
                                (Tuple.mapSecond <|
                                    RemoteData.map <|
                                        Phrasebook.updatePhrase phraseId value
                                )
                                model.phrasebook
                        , currentPhrase = phraseId
                    }
            in
            ( newModel, savePhrasebook newModel )

        ClickedAddPhrase ->
            case model.phrasebook of
                Just ( phrasebookId, phrasebook ) ->
                    let
                        id : PhraseId
                        id =
                            RemoteData.map Phrasebook.newPhraseId phrasebook
                                |> RemoteData.unwrapWithDefault 0
                    in
                    ( { model
                        | phrasebook =
                            Just
                                ( phrasebookId
                                , RemoteData.map
                                    (Phrasebook.clean
                                        >> (::) ( id, "" )
                                    )
                                    phrasebook
                                )
                        , currentPhrase = id
                      }
                    , Browser.Dom.focus (domId id) |> Task.attempt (\_ -> NoOp)
                    )

                _ ->
                    ( model, Cmd.none )

        ClickedRemovePhrase id ->
            let
                newModel : FrontendModel
                newModel =
                    { model
                        | phrasebook =
                            Maybe.map
                                (Tuple.mapSecond <|
                                    RemoteData.map <|
                                        Phrasebook.removePhrase id
                                )
                                model.phrasebook
                    }
            in
            ( newModel, savePhrasebook newModel )

        HitArrowKeyUpFrom phraseId ->
            case model.phrasebook of
                Just ( _, Loaded phrasebook ) ->
                    let
                        targetPhraseId : Maybe PhraseId
                        targetPhraseId =
                            Phrasebook.previousPhraseId phraseId phrasebook
                    in
                    case targetPhraseId of
                        Just id ->
                            ( model, Browser.Dom.focus (domId id) |> Task.attempt (\_ -> NoOp) )

                        Nothing ->
                            ( model, Cmd.none )

                _ ->
                    ( model, Cmd.none )

        HitArrowKeyDownFrom phraseId ->
            case model.phrasebook of
                Just ( _, Loaded phrasebook ) ->
                    let
                        targetPhraseId : Maybe PhraseId
                        targetPhraseId =
                            Phrasebook.nextPhraseId phraseId phrasebook
                    in
                    case targetPhraseId of
                        Just id ->
                            ( model, Browser.Dom.focus (domId id) |> Task.attempt (\_ -> NoOp) )

                        Nothing ->
                            ( model, Cmd.none )

                _ ->
                    ( model, Cmd.none )

        TypedImportText importText ->
            ( { model | importText = importText }, Cmd.none )

        CancelledImportReplace ->
            ( { model | importReplaceNeedsConfirm = False }, Cmd.none )

        ClickedImportReplace ->
            ( { model | importReplaceNeedsConfirm = True }, Cmd.none )

        ConfirmedImportReplace ->
            case model.route of
                Route.Phrasebook phrasebookId _ ->
                    let
                        phrases : List String
                        phrases =
                            prepareImport model.importText

                        newModel : FrontendModel
                        newModel =
                            { model
                                | phrasebook =
                                    Maybe.map
                                        (Tuple.mapSecond <|
                                            \_ ->
                                                Phrasebook.addPhrases phrases Phrasebook.new
                                                    |> RemoteData.fromData
                                        )
                                        model.phrasebook
                                , importText = ""
                                , importReplaceNeedsConfirm = False
                            }

                        practiceUrl : String
                        practiceUrl =
                            Route.toAbsoluteUrl [ Route.practiceCodec ] (Route.Phrasebook phrasebookId Practice)
                    in
                    ( newModel
                    , Cmd.batch
                        [ Browser.Navigation.pushUrl model.key practiceUrl
                        , savePhrasebook newModel
                        , getRandomPhrase newModel
                        ]
                    )

                _ ->
                    ( model, Cmd.none )

        ClickedImportAppend ->
            case model.route of
                Route.Phrasebook phrasebookId _ ->
                    let
                        phrases : List String
                        phrases =
                            prepareImport model.importText

                        newModel : FrontendModel
                        newModel =
                            { model
                                | phrasebook =
                                    Maybe.map
                                        (Tuple.mapSecond <|
                                            RemoteData.map
                                                (\existingPhrasebook -> Phrasebook.addPhrases phrases existingPhrasebook)
                                        )
                                        model.phrasebook
                                , importText = ""
                            }

                        practiceUrl : String
                        practiceUrl =
                            Route.toAbsoluteUrl [ Route.practiceCodec ] (Route.Phrasebook phrasebookId Practice)
                    in
                    ( newModel
                    , Cmd.batch
                        [ Browser.Navigation.pushUrl model.key practiceUrl
                        , savePhrasebook newModel
                        , getRandomPhrase newModel
                        ]
                    )

                _ ->
                    ( model, Cmd.none )

        ClickedCopyId ->
            case model.route of
                Route.Phrasebook phrasebookId _ ->
                    ( model, copy phrasebookId )

                _ ->
                    ( model, Cmd.none )

        ClickedCopyAddress ->
            case model.route of
                Route.Phrasebook phrasebookId _ ->
                    ( model, copyAddress phrasebookId )

                _ ->
                    ( model, Cmd.none )

        ClickedCopyPhrases ->
            case model.phrasebook of
                Just ( _, Loaded phrases ) ->
                    ( model, copy (Phrasebook.toString phrases) )

                _ ->
                    ( model, Cmd.none )

        BlurredPhraseEditorInput phraseId ->
            case model.phrasebook of
                Just ( id, Loaded phrasebook ) ->
                    if String.isEmpty (Phrasebook.getPhrase phraseId phrasebook) then
                        let
                            updatedModel =
                                { model | phrasebook = Just ( id, Loaded (Phrasebook.clean phrasebook) ) }
                        in
                        ( updatedModel, getRandomPhrase updatedModel )

                    else
                        ( model, Cmd.none )

                _ ->
                    ( model, Cmd.none )

        FocusedPhraseEditorInput phraseId ->
            ( { model | currentPhrase = phraseId }, Cmd.none )

        Tick newTime ->
            let
                model_ =
                    case model.animationToRunAtNextTick of
                        Just { current, previous } ->
                            { model
                                | currentPhrase = current
                                , previousPhrase = Just previous
                            }

                        Nothing ->
                            model

                model__ =
                    { model_ | ticking = not <| Animator.arrivedAt True newTime model.animationDone }
            in
            ( model__ |> Animator.update newTime animator, Cmd.none )

        NoOp ->
            ( model, Cmd.none )


setRoute : Route.Route -> FrontendModel -> FrontendModel
setRoute route model =
    { model | route = route }


gotoRoute : Route.Route -> FrontendModel -> ( FrontendModel, Cmd FrontendMsg )
gotoRoute route model =
    model
        |> setRoute route
        |> (\m ->
                case m.route of
                    Route.Phrasebook phrasebookId page ->
                        ( { m
                            | phrasebook =
                                Just
                                    ( phrasebookId
                                    , Maybe.map Tuple.second m.phrasebook |> Maybe.withDefault Initial
                                    )
                          }
                        , case page of
                            Editor ->
                                Browser.Dom.focus (domId m.currentPhrase) |> Task.attempt (\_ -> NoOp)

                            Import ->
                                Browser.Dom.focus importId |> Task.attempt (\_ -> NoOp)

                            _ ->
                                Cmd.none
                        )
                            |> loadPhrasebookIfNeeded

                    _ ->
                        ( m, Cmd.none )
           )


loadPhrasebookIfNeeded : ( FrontendModel, Cmd FrontendMsg ) -> ( FrontendModel, Cmd FrontendMsg )
loadPhrasebookIfNeeded ( model, cmds ) =
    case model.phrasebook of
        Just ( _, Loaded _ ) ->
            ( model, cmds )

        Just ( phrasebookId, _ ) ->
            ( { model | phrasebook = Just ( phrasebookId, Loading ), currentPhrase = 0 }
            , Cmd.batch [ cmds, Lamdera.sendToBackend (LoadPhrasebook phrasebookId) ]
            )

        Nothing ->
            ( model, cmds )


getRandomPhrase : FrontendModel -> Cmd FrontendMsg
getRandomPhrase model =
    case model.phrasebook of
        Just ( _, Loaded phrases ) ->
            Random.generate GotPhrase (Phrasebook.randomPhraseGenerator phrases model.currentPhrase)

        _ ->
            Cmd.none


savePhrasebook : FrontendModel -> Cmd FrontendMsg
savePhrasebook model =
    case model.phrasebook of
        Just ( id, Loaded phrases ) ->
            Lamdera.sendToBackend (SavePhrasebook id phrases)

        _ ->
            Cmd.none


domId : Int -> String
domId id =
    "phrase-" ++ String.fromInt id


updateFromBackend : ToFrontend -> FrontendModel -> ( FrontendModel, Cmd FrontendMsg )
updateFromBackend msg model =
    case msg of
        GotPhrasebook phrasebookId phrasebook ->
            { model | phrasebook = Just ( phrasebookId, Loaded phrasebook ) }
                |> update AskedPhrase

        GotNewPhrasebookId phrasebookId ->
            let
                phrasebookUrl : String
                phrasebookUrl =
                    Route.toAbsoluteUrl [ Route.practiceCodec ] (Route.Phrasebook phrasebookId Practice)
            in
            ( model, Browser.Navigation.pushUrl model.key phrasebookUrl )

        SavedPhrasebook ->
            ( model, Cmd.none )


prepareImport : String -> List String
prepareImport importText =
    importText
        |> String.split "\n"
        |> List.map String.trim
        |> List.filter (not << String.isEmpty)
        -- Reverse the order of lines so that they keep their order
        -- after being prepended one by one
        |> List.reverse


view : FrontendModel -> Element FrontendMsg
view model =
    case ( model.route, model.phrasebook ) of
        ( Route.Landing, _ ) ->
            viewHome

        ( Route.Phrasebook _ page, Just ( phrasebookId, remoteDataPhrasebook ) ) ->
            viewPhrasebook phrasebookId remoteDataPhrasebook page model

        _ ->
            El.text "Erreur : pas de phrasebook"


viewHome : Element FrontendMsg
viewHome =
    El.el [ El.centerX, El.centerY ] <|
        Input.button
            buttonStyle
            { onPress = Just ClickedCreatePhrasebook, label = El.el [] (El.text "Créer une liste de phrases") }


viewPhrasebook : PhrasebookId -> RemoteData err Phrasebook -> Page -> FrontendModel -> Element FrontendMsg
viewPhrasebook phrasebookId phrasebook page model =
    case page of
        Practice ->
            viewPractice phrasebook model.currentPhrase model.previousPhrase phrasebookId model.animationDone

        Editor ->
            viewEditor (RemoteData.unwrapWithDefault [] phrasebook) phrasebookId

        Share ->
            viewShare phrasebookId

        Import ->
            viewImport
                model.importText
                model.importReplaceNeedsConfirm
                (Maybe.map Tuple.first model.phrasebook |> Maybe.withDefault "")


viewPractice : RemoteData err Phrasebook -> Int -> Maybe Int -> PhrasebookId -> Animator.Timeline Bool -> Element FrontendMsg
viewPractice phrasebook currentPhrase previousPhrase phrasebookId timeline =
    El.column
        [ El.height El.fill, El.width El.fill ]
        [ El.column [ El.width El.fill, El.height El.fill, El.htmlAttribute <| Attr.style "justify-content" "space-evenly" ]
            [ case phrasebook of
                Initial ->
                    El.el [ Font.size 32, El.centerX, Font.italic ] (El.text <| "Initialisation")

                Loading ->
                    El.el [ Font.size 16, El.centerX, Font.italic ] (El.text <| "…")

                Loaded phrasebook_ ->
                    viewPracticeWithPhrasebook phrasebook_ currentPhrase previousPhrase timeline

                Errored _ ->
                    El.text "Erreur !"
            , Input.button
                practiceButtonStyle
                { onPress = Just AskedPhrase, label = El.el [] (El.text "Ar-ti-cu-le\u{202F}!") }
            ]
        , El.row [ El.padding 20, El.centerX ]
            [ El.link buttonStyle
                { url =
                    Route.toAbsoluteUrl [ Route.editorCodec ] (Route.Phrasebook phrasebookId Editor)
                , label = El.text "Éditer les phrases"
                }
            ]
        ]


viewPracticeWithPhrasebook : Phrasebook -> Int -> Maybe Int -> Animator.Timeline Bool -> Element FrontendMsg
viewPracticeWithPhrasebook phrasebook currentPhraseId maybePreviousPhraseId timeline =
    case phrasebook of
        [] ->
            El.el [ Font.size 32, El.centerX, Font.italic ] (El.text <| "(aucune phrase)")

        _ ->
            let
                currentPhrase =
                    Phrasebook.getPhrase currentPhraseId phrasebook

                previousPhrase =
                    case maybePreviousPhraseId of
                        Just previousPhraseId ->
                            Phrasebook.getPhrase previousPhraseId phrasebook

                        Nothing ->
                            ""

                viewSingleWord : String -> { from : Float, to : Float } -> { from : Float, to : Float } -> Bool -> Html.Html FrontendMsg
                viewSingleWord word x opacity hidden =
                    Html.div
                        [ Animator.Inline.xy
                            timeline
                            (\animationDone ->
                                if not <| animationDone then
                                    { x = Animator.at x.from, y = Animator.at 0 }

                                else
                                    { x = Animator.at x.to, y = Animator.at 0 }
                            )
                        , Animator.Inline.opacity
                            timeline
                            (\animationDone ->
                                if not <| animationDone then
                                    Animator.at opacity.from

                                else
                                    Animator.at opacity.to
                            )
                        , Attr.style "width" "100%"
                        , Attr.style "white-space" "wrap"
                        , Attr.style "height" "3em"
                        , Attr.style "overflow-y" "auto"
                        , if hidden then
                            Attr.style "position" "absolute"

                          else
                            Attr.style "" ""
                        , Attr.style "top" "0"
                        , Attr.style "text-align" "center"
                        , Attr.style "font-weight" "bold"
                        , Attr.style "font-size" "2rem"
                        , Attr.attribute "aria-hidden" (boolToString hidden)
                        ]
                        [ Html.span [ Attr.style "opacity" ".5" ] [ Html.text "«\u{00A0}" ]
                        , Html.text word
                        , Html.span [ Attr.style "opacity" ".5" ] [ Html.text "\u{00A0}»" ]
                        ]
            in
            El.html
                (Html.div
                    [ Attr.style "position" "relative", Attr.style "width" "100%", Attr.style "overflow" "hidden" ]
                    [ viewSingleWord currentPhrase { from = -400, to = 0 } { from = 0, to = 1 } False
                    , viewSingleWord previousPhrase { from = 0, to = 400 } { from = 1, to = 0 } True
                    ]
                )


viewEditor : Phrasebook -> PhrasebookId -> Element FrontendMsg
viewEditor phrasebook phrasebookId =
    El.column [ El.height El.fill, El.width El.fill ]
        [ editorPhrases phrasebook
        , editorActions phrasebookId
        ]


viewShare : PhrasebookId -> Element FrontendMsg
viewShare phrasebookId =
    El.column [ El.height El.fill, El.width El.fill ]
        [ El.column
            [ El.centerX, El.centerY, El.padding 20, El.spacing 30, El.htmlAttribute <| Attr.id exportId ]
            [ El.paragraph []
                [ El.text "L’identifiant de votre liste de phrases est "
                , El.el [ El.centerX, Font.bold, Font.size 20 ] (El.text phrasebookId)
                , El.text "."
                ]
            , El.row [ El.centerX, El.spacing 30 ]
                [ Input.button
                    buttonStyle
                    { onPress = Just ClickedCopyId, label = El.text "Copier l’identifiant" }
                , Input.button
                    buttonStyle
                    { onPress = Just ClickedCopyAddress, label = El.text "Copier l’adresse" }
                ]
            , Input.button
                buttonStyle
                { onPress = Just ClickedCopyPhrases, label = El.text "Copier les phrases" }
            ]
        , El.row
            [ El.padding 20, El.spacing 20, El.centerX ]
            [ El.link buttonStyle
                { url =
                    Route.toAbsoluteUrl [ Route.editorCodec ] (Route.Phrasebook phrasebookId Editor)
                , label = El.text "Retour"
                }
            ]
        ]


viewImport : String -> Bool -> PhrasebookId -> Element FrontendMsg
viewImport importText importReplaceNeedsConfirm phrasebookId =
    El.column [ El.height El.fill, El.width El.fill ]
        [ El.html <|
            Html.textarea
                [ Attr.value importText
                , Attr.id importId
                , Events.onInput TypedImportText
                , Attr.style "flex-grow" "1"
                , Attr.style "width" "100%"
                , Attr.style "box-sizing" "border-box"
                , Attr.style "font-family" "inherit"
                , Attr.style "font-size" "inherit"
                , Attr.style "color" "inherit"
                , Attr.style "padding" ".9em"
                , Attr.style "line-height" "1.5"
                , Attr.style "resize" "none"
                , Attr.style "border" "none"
                , Attr.placeholder "Copiez ici les phrases à importer (une par ligne)."
                , Attr.attribute "aria-label" importLabel
                ]
                []
        , El.row [ El.padding 20, El.centerX ]
            [ El.wrappedRow [ El.spacing 20, El.width El.fill ]
                [ Input.button
                    ((El.htmlAttribute <| Events.onBlur CancelledImportReplace)
                        :: (El.htmlAttribute <| onKeyUp [ ( "Escape", CancelledImportReplace ) ])
                        :: buttonStyle
                    )
                    { onPress =
                        Just <|
                            if importReplaceNeedsConfirm then
                                ConfirmedImportReplace

                            else
                                ClickedImportReplace
                    , label =
                        if importReplaceNeedsConfirm then
                            El.text "Confirmer ?"

                        else
                            El.text "Remplacer les phrases"
                    }
                , Input.button
                    buttonStyle
                    { onPress = Just <| ClickedImportAppend
                    , label = El.text "Ajouter les phrases"
                    }
                , El.link buttonStyle
                    { url =
                        Route.toAbsoluteUrl [ Route.editorCodec ] (Route.Phrasebook phrasebookId Editor)
                    , label = El.text "Retour"
                    }
                ]
            ]
        ]


exportId : String
exportId =
    "export-phrases-list"


importId : String
importId =
    "import-phrases-text"


importLabel : String
importLabel =
    "Phrases à importer (une par ligne)"


editorActions : PhrasebookId -> Element FrontendMsg
editorActions phrasebookId =
    El.row [ El.padding 20, El.centerX ]
        [ El.wrappedRow [ El.spacing 20, El.width El.fill ]
            [ Input.button
                buttonStyle
                { onPress = Just ClickedAddPhrase
                , label = El.el [ Region.description "Ajouter une phrase" ] (El.text "Ajouter")
                }
            , El.link buttonStyle
                { url =
                    Route.toAbsoluteUrl [ Route.importCodec ] (Route.Phrasebook phrasebookId Import)
                , label = El.text "Importer"
                }
            , El.link buttonStyle
                { url =
                    Route.toAbsoluteUrl [ Route.shareCodec ] (Route.Phrasebook phrasebookId Share)
                , label = El.text "Partager"
                }
            , El.link buttonStyle
                { url =
                    Route.toAbsoluteUrl [ Route.practiceCodec ] (Route.Phrasebook phrasebookId Practice)
                , label = El.text "Retour"
                }
            ]
        ]


editorPhrases : Phrasebook -> Element FrontendMsg
editorPhrases phrases =
    El.column
        [ El.centerX
        , El.centerY
        , El.width El.fill
        , El.height El.fill
        , El.padding 20

        -- Some spacing is needed in order to let the outline be fully visible
        , El.spacing 4
        , El.scrollbarY
        ]
        (List.map singlePhraseEditor phrases)


singlePhraseEditor : ( PhraseId, Phrase ) -> Element FrontendMsg
singlePhraseEditor ( id, phrase ) =
    El.row
        [ El.width El.fill, El.spacing 10 ]
        [ Input.text
            [ El.centerX
            , Border.width 0
            , El.htmlAttribute <| Attr.id (domId id)
            , El.htmlAttribute <| Events.onBlur (BlurredPhraseEditorInput id)
            , El.htmlAttribute <| Events.onFocus (FocusedPhraseEditorInput id)
            , El.htmlAttribute <|
                onKeyUp
                    [ ( "ArrowDown", HitArrowKeyDownFrom id )
                    , ( "ArrowUp", HitArrowKeyUpFrom id )
                    , ( "Enter", ClickedAddPhrase )
                    ]
            ]
            { onChange = TypedPhrase id
            , text = phrase
            , placeholder = Nothing
            , label = Input.labelHidden "Phrase"
            }
        , Input.button
            [ El.padding 10
            , Font.size 25
            , Border.rounded 25
            , El.mouseOver [ Font.color paper, Background.color ink ]
            , if Phrase.isEmpty phrase then
                El.htmlAttribute (Attr.style "visibility" "hidden")

              else
                El.htmlAttribute (Attr.style "" "")
            ]
            { onPress = Just <| ClickedRemovePhrase id
            , label = El.el [ Region.description "Supprimer la phrase" ] (El.text "×")
            }
        ]


onKeyUp : List ( String, msg ) -> Html.Attribute msg
onKeyUp keysWithMsgs =
    Events.on "keyup"
        (Dec.field "key" Dec.string
            |> Dec.andThen
                (\key_ ->
                    case List.filter (\( k, _ ) -> k == key_) keysWithMsgs of
                        [ ( _, msg ) ] ->
                            Dec.succeed msg

                        _ ->
                            Dec.fail key_
                )
        )


buttonStyle : List (El.Attribute msg)
buttonStyle =
    [ El.centerX
    , Border.width 2
    , Border.rounded 25
    , El.padding 12
    , El.mouseOver [ Font.color paper, Background.color ink, Border.color ink ]
    , El.htmlAttribute <| Attr.style "text-transform" "uppercase"
    , Font.size 16
    , Font.letterSpacing 0.5
    , Font.bold
    ]


practiceButtonStyle : List (El.Attribute FrontendMsg)
practiceButtonStyle =
    [ Font.bold, Font.color paper, Background.color ink, Border.color ink ]
        ++ buttonStyle


ink : El.Color
ink =
    El.rgb255 41 60 75


paper : El.Color
paper =
    El.rgb 1 1 1


boolToString : Bool -> String
boolToString boolean =
    if boolean then
        "true"

    else
        "false"


subscriptions : FrontendModel -> Sub FrontendMsg
subscriptions model =
    if model.ticking then
        animator
            |> Animator.toSubscription Tick model

    else
        Sub.none


port copy : String -> Cmd msg


port copyAddress : String -> Cmd msg
