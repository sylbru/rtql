module Backend exposing (app)

import Dict
import Lamdera exposing (ClientId, SessionId)
import Phrasebook
import Random
import Task
import Types exposing (BackendModel, BackendMsg(..), ToBackend(..), ToFrontend(..))


app :
    { init : ( BackendModel, Cmd BackendMsg )
    , update : BackendMsg -> BackendModel -> ( BackendModel, Cmd BackendMsg )
    , updateFromFrontend : SessionId -> ClientId -> ToBackend -> BackendModel -> ( BackendModel, Cmd BackendMsg )
    , subscriptions : BackendModel -> Sub BackendMsg
    }
app =
    Lamdera.backend
        { init = init
        , update = update
        , updateFromFrontend = updateFromFrontend
        , subscriptions = subscriptions
        }


init : ( BackendModel, Cmd BackendMsg )
init =
    ( { phrasebooks = Dict.empty
      , seed = Random.initialSeed 42
      }
    , Task.perform (\_ -> NoOpBackendMsg) (Task.succeed ())
    )


update : BackendMsg -> BackendModel -> ( BackendModel, Cmd BackendMsg )
update _ model =
    ( model, Cmd.none )


updateFromFrontend : SessionId -> ClientId -> ToBackend -> BackendModel -> ( BackendModel, Cmd BackendMsg )
updateFromFrontend _ clientId toBackend model =
    case toBackend of
        LoadPhrasebook phrasebookId ->
            case Dict.get phrasebookId model.phrasebooks of
                Just phrasebook ->
                    ( model, Lamdera.sendToFrontend clientId (GotPhrasebook phrasebookId phrasebook) )

                Nothing ->
                    ( model, Cmd.none )

        SavePhrasebook phrasebookId phrasebook ->
            ( { model | phrasebooks = Dict.insert phrasebookId phrasebook model.phrasebooks }
            , Lamdera.sendToFrontend clientId SavedPhrasebook
            )

        CreatePhrasebook ->
            let
                ( phrasebookId, newSeed ) =
                    Random.step Phrasebook.idGenerator model.seed
            in
            ( { model
                | phrasebooks = Dict.insert phrasebookId Phrasebook.new model.phrasebooks
                , seed = newSeed
              }
            , Lamdera.sendToFrontend clientId (GotNewPhrasebookId phrasebookId)
            )


subscriptions : BackendModel -> Sub BackendMsg
subscriptions _ =
    Sub.none
