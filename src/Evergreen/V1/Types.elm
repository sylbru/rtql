module Evergreen.V1.Types exposing (..)

import Browser
import Browser.Navigation
import Url


type alias Phrase =
    ( Int, String )


type Page
    = Practice
    | Editor
    | Export
    | Import String


type alias FrontendModel =
    { key : Browser.Navigation.Key
    , phrases : List Phrase
    , currentPhrase : Int
    , page : Page
    }


type alias BackendModel =
    {}


type FrontendMsg
    = UrlClicked Browser.UrlRequest
    | UrlChanged Url.Url
    | AskedPhrase
    | GotPhrase Int
    | TypedPhrase Int String
    | ClickedAddPhrase
    | ClickedRemovePhrase Int
    | HitArrowKeyTowardsPhrase Int
    | To Page
    | TypedImportText String
    | ClickedImportReplace (List String)
    | ClickedImportAppend (List String)
    | ClickedCopy
    | NoOp


type ToBackend
    = NoOpToBackend


type BackendMsg
    = NoOpBackendMsg


type ToFrontend
    = NoOpToFrontend
