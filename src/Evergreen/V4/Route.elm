module Evergreen.V4.Route exposing (..)

import Evergreen.V4.Phrasebook


type Page
    = Practice
    | Editor
    | Share
    | Import


type Route
    = Landing
    | Phrasebook Evergreen.V4.Phrasebook.PhrasebookId Page
