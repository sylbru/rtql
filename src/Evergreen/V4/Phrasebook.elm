module Evergreen.V4.Phrasebook exposing (..)

import Evergreen.V4.Phrase


type alias PhrasebookId =
    String


type alias Phrasebook =
    List Evergreen.V4.Phrase.Phrase
