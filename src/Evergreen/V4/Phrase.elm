module Evergreen.V4.Phrase exposing (..)


type alias Phrase =
    ( Int, String )
