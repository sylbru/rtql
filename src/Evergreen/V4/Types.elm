module Evergreen.V4.Types exposing (..)

import Browser
import Browser.Navigation
import Dict
import Evergreen.V4.Phrasebook
import Evergreen.V4.RemoteData
import Evergreen.V4.Route
import Http
import Random
import Url


type alias FrontendModel =
    { key : Browser.Navigation.Key
    , phrasebook : Maybe ( Evergreen.V4.Phrasebook.PhrasebookId, Evergreen.V4.RemoteData.RemoteData Http.Error Evergreen.V4.Phrasebook.Phrasebook )
    , currentPhrase : Int
    , importText : String
    , route : Evergreen.V4.Route.Route
    }


type alias BackendModel =
    { phrasebooks : Dict.Dict Evergreen.V4.Phrasebook.PhrasebookId Evergreen.V4.Phrasebook.Phrasebook
    , seed : Random.Seed
    }


type FrontendMsg
    = UrlClicked Browser.UrlRequest
    | UrlChanged Url.Url
    | ClickedCreatePhrasebook
    | AskedPhrase
    | GotPhrase Int
    | TypedPhrase Int String
    | ClickedAddPhrase
    | ClickedRemovePhrase Int
    | HitArrowKeyTowardsPhrase Int
    | TypedImportText String
    | ClickedImportReplace Evergreen.V4.Phrasebook.PhrasebookId (List String)
    | ClickedImportAppend Evergreen.V4.Phrasebook.PhrasebookId (List String)
    | ClickedCopyId
    | ClickedCopyAddress
    | ClickedCopyPhrases
    | NoOp


type ToBackend
    = LoadPhrasebook Evergreen.V4.Phrasebook.PhrasebookId
    | SavePhrasebook Evergreen.V4.Phrasebook.PhrasebookId Evergreen.V4.Phrasebook.Phrasebook
    | CreatePhrasebook


type BackendMsg
    = NoOpBackendMsg


type ToFrontend
    = GotPhrasebook Evergreen.V4.Phrasebook.PhrasebookId Evergreen.V4.Phrasebook.Phrasebook
    | GotNewPhrasebookId Evergreen.V4.Phrasebook.PhrasebookId
