module Evergreen.Migrate.V6 exposing (..)

import Evergreen.V5.Types as Old
import Evergreen.V6.Types as New
import Lamdera.Migrations exposing (..)


frontendModel : Old.FrontendModel -> ModelMigration New.FrontendModel New.FrontendMsg
frontendModel old =
    ModelUnchanged


backendModel : Old.BackendModel -> ModelMigration New.BackendModel New.BackendMsg
backendModel old =
    ModelUnchanged


frontendMsg : Old.FrontendMsg -> MsgMigration New.FrontendMsg New.FrontendMsg
frontendMsg old =
    case old of
        Old.ClickedImportReplace id _ ->
            MsgMigrated ( New.ClickedImportReplace id, Cmd.none )

        Old.ClickedImportAppend id _ ->
            MsgMigrated ( New.ClickedImportAppend id, Cmd.none )

        Old.BlurredPhraseEditorInput ->
            MsgMigrated ( New.BlurredPhraseEditorInput 0, Cmd.none )

        _ ->
            MsgUnchanged


toBackend : Old.ToBackend -> MsgMigration New.ToBackend New.BackendMsg
toBackend old =
    MsgUnchanged


backendMsg : Old.BackendMsg -> MsgMigration New.BackendMsg New.BackendMsg
backendMsg old =
    MsgUnchanged


toFrontend : Old.ToFrontend -> MsgMigration New.ToFrontend New.FrontendMsg
toFrontend old =
    MsgUnchanged
