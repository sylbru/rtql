module Evergreen.Migrate.V7 exposing (..)

import Animator
import Evergreen.V6.RemoteData
import Evergreen.V6.Route
import Evergreen.V6.Types
import Evergreen.V7.Animator
import Evergreen.V7.RemoteData
import Evergreen.V7.Route
import Evergreen.V7.Types
import Internal.Time as Time
import Internal.Timeline as Timeline
import Lamdera.Migrations exposing (..)
import Maybe
import Time


frontendModel : Evergreen.V6.Types.FrontendModel -> ModelMigration Evergreen.V7.Types.FrontendModel Evergreen.V7.Types.FrontendMsg
frontendModel old =
    ModelUnchanged


backendModel : Evergreen.V6.Types.BackendModel -> ModelMigration Evergreen.V7.Types.BackendModel Evergreen.V7.Types.BackendMsg
backendModel old =
    ModelUnchanged


frontendMsg : Evergreen.V6.Types.FrontendMsg -> MsgMigration Evergreen.V7.Types.FrontendMsg Evergreen.V7.Types.FrontendMsg
frontendMsg old =
    MsgUnchanged


toBackend : Evergreen.V6.Types.ToBackend -> MsgMigration Evergreen.V7.Types.ToBackend Evergreen.V7.Types.BackendMsg
toBackend old =
    MsgUnchanged


backendMsg : Evergreen.V6.Types.BackendMsg -> MsgMigration Evergreen.V7.Types.BackendMsg Evergreen.V7.Types.BackendMsg
backendMsg old =
    MsgUnchanged


toFrontend : Evergreen.V6.Types.ToFrontend -> MsgMigration Evergreen.V7.Types.ToFrontend Evergreen.V7.Types.FrontendMsg
toFrontend old =
    MsgUnchanged
