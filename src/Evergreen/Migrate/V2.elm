module Evergreen.Migrate.V2 exposing (..)

import Dict
import Evergreen.V1.Types as Old
import Evergreen.V2.Route
import Evergreen.V2.Types as New
import Lamdera.Migrations exposing (..)


frontendModel : Old.FrontendModel -> ModelMigration New.FrontendModel New.FrontendMsg
frontendModel old =
    let
        importText =
            case old.page of
                Old.Import oldImportText ->
                    oldImportText

                _ ->
                    ""
    in
    ModelMigrated
        ( { key = old.key
          , phrasebook = Nothing
          , currentPhrase = old.currentPhrase
          , importText = importText
          , route = Evergreen.V2.Route.Landing
          }
        , Cmd.none
        )


backendModel : Old.BackendModel -> ModelMigration New.BackendModel New.BackendMsg
backendModel old =
    ModelMigrated ( Dict.empty, Cmd.none )


frontendMsg : Old.FrontendMsg -> MsgMigration New.FrontendMsg New.FrontendMsg
frontendMsg old =
    case old of
        Old.UrlClicked u ->
            MsgMigrated ( New.UrlClicked u, Cmd.none )

        Old.UrlChanged u ->
            MsgMigrated ( New.UrlChanged u, Cmd.none )

        Old.AskedPhrase ->
            MsgMigrated ( New.AskedPhrase, Cmd.none )

        Old.GotPhrase i ->
            MsgMigrated ( New.GotPhrase i, Cmd.none )

        Old.TypedPhrase i s ->
            MsgMigrated ( New.TypedPhrase i s, Cmd.none )

        Old.ClickedAddPhrase ->
            MsgMigrated ( New.ClickedAddPhrase, Cmd.none )

        Old.ClickedRemovePhrase i ->
            MsgMigrated ( New.ClickedRemovePhrase i, Cmd.none )

        Old.HitArrowKeyTowardsPhrase i ->
            MsgMigrated ( New.HitArrowKeyTowardsPhrase i, Cmd.none )

        Old.To _ ->
            MsgOldValueIgnored

        Old.TypedImportText s ->
            MsgMigrated <| ( New.TypedImportText s, Cmd.none )

        Old.ClickedImportReplace ss ->
            MsgOldValueIgnored

        Old.ClickedImportAppend ss ->
            MsgOldValueIgnored

        Old.ClickedCopy ->
            MsgOldValueIgnored

        Old.NoOp ->
            MsgMigrated <| ( New.NoOp, Cmd.none )


toBackend : Old.ToBackend -> MsgMigration New.ToBackend New.BackendMsg
toBackend old =
    MsgOldValueIgnored


backendMsg : Old.BackendMsg -> MsgMigration New.BackendMsg New.BackendMsg
backendMsg old =
    MsgUnchanged


toFrontend : Old.ToFrontend -> MsgMigration New.ToFrontend New.FrontendMsg
toFrontend old =
    MsgOldValueIgnored
