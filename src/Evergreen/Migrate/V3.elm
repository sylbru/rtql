module Evergreen.Migrate.V3 exposing (..)

import Dict
import Evergreen.V2.RemoteData
import Evergreen.V2.Route
import Evergreen.V2.Types as Old
import Evergreen.V3.Phrasebook
import Evergreen.V3.RemoteData
import Evergreen.V3.Route
import Evergreen.V3.Types as New
import Http
import Lamdera.Migrations exposing (..)
import Random


frontendModel : Old.FrontendModel -> ModelMigration New.FrontendModel New.FrontendMsg
frontendModel old =
    let
        migrateRemoteData : Evergreen.V2.RemoteData.RemoteData e d -> Evergreen.V3.RemoteData.RemoteData e d
        migrateRemoteData oldRemoteData =
            case oldRemoteData of
                Evergreen.V2.RemoteData.Initial ->
                    Evergreen.V3.RemoteData.Initial

                Evergreen.V2.RemoteData.Loading ->
                    Evergreen.V3.RemoteData.Loading

                Evergreen.V2.RemoteData.Loaded data ->
                    Evergreen.V3.RemoteData.Loaded data

                Evergreen.V2.RemoteData.Errored error ->
                    Evergreen.V3.RemoteData.Errored error

        newCurrentPhrasebook : Maybe ( Evergreen.V3.Phrasebook.PhrasebookId, Evergreen.V3.RemoteData.RemoteData Http.Error Evergreen.V3.Phrasebook.Phrasebook )
        newCurrentPhrasebook =
            old.phrasebook
                |> Maybe.map (Tuple.mapFirst String.fromInt)
                |> Maybe.map (Tuple.mapSecond (always Evergreen.V3.RemoteData.Initial))

        newRoute =
            case old.route of
                Evergreen.V2.Route.Landing ->
                    Evergreen.V3.Route.Landing

                Evergreen.V2.Route.Phrasebook id page ->
                    let
                        newPage =
                            case page of
                                Evergreen.V2.Route.Practice ->
                                    Evergreen.V3.Route.Practice

                                Evergreen.V2.Route.Editor ->
                                    Evergreen.V3.Route.Editor

                                Evergreen.V2.Route.Import ->
                                    Evergreen.V3.Route.Import

                                Evergreen.V2.Route.Export ->
                                    Evergreen.V3.Route.Share
                    in
                    Evergreen.V3.Route.Phrasebook (String.fromInt id) newPage
    in
    ModelMigrated
        ( { key = old.key
          , phrasebook = newCurrentPhrasebook
          , currentPhrase = old.currentPhrase
          , importText = old.importText
          , route = newRoute
          }
        , Cmd.none
        )


backendModel : Old.BackendModel -> ModelMigration New.BackendModel New.BackendMsg
backendModel old =
    let
        newPhrasebooks =
            old
                |> Dict.toList
                |> List.map (Tuple.mapFirst String.fromInt)
                |> Dict.fromList
    in
    ModelMigrated ( { phrasebooks = newPhrasebooks, seed = Random.initialSeed 42 }, Cmd.none )


frontendMsg : Old.FrontendMsg -> MsgMigration New.FrontendMsg New.FrontendMsg
frontendMsg old =
    MsgOldValueIgnored


toBackend : Old.ToBackend -> MsgMigration New.ToBackend New.BackendMsg
toBackend old =
    MsgOldValueIgnored


backendMsg : Old.BackendMsg -> MsgMigration New.BackendMsg New.BackendMsg
backendMsg old =
    MsgUnchanged


toFrontend : Old.ToFrontend -> MsgMigration New.ToFrontend New.FrontendMsg
toFrontend old =
    MsgOldValueIgnored
