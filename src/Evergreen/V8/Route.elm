module Evergreen.V8.Route exposing (..)

import Evergreen.V8.Phrasebook


type Page
    = Practice
    | Editor
    | Share
    | Import


type Route
    = Landing
    | Phrasebook Evergreen.V8.Phrasebook.PhrasebookId Page
