module Evergreen.V8.Phrase exposing (..)


type alias PhraseId =
    Int


type alias Phrase =
    String
