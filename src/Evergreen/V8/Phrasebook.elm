module Evergreen.V8.Phrasebook exposing (..)

import Evergreen.V8.Phrase


type alias PhrasebookId =
    String


type alias Phrasebook =
    List ( Evergreen.V8.Phrase.PhraseId, Evergreen.V8.Phrase.Phrase )
