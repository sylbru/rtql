module Evergreen.V8.Animator exposing (..)

import Evergreen.V8.Internal.Timeline


type alias Timeline state =
    Evergreen.V8.Internal.Timeline.Timeline state
