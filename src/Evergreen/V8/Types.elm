module Evergreen.V8.Types exposing (..)

import Browser
import Browser.Navigation
import Dict
import Evergreen.V8.Animator
import Evergreen.V8.Phrase
import Evergreen.V8.Phrasebook
import Evergreen.V8.RemoteData
import Evergreen.V8.Route
import Http
import Random
import Time
import Url


type alias FrontendModel =
    { key : Browser.Navigation.Key
    , phrasebook : Maybe ( Evergreen.V8.Phrasebook.PhrasebookId, Evergreen.V8.RemoteData.RemoteData Http.Error Evergreen.V8.Phrasebook.Phrasebook )
    , currentPhrase : Int
    , previousPhrase : Maybe Int
    , animationToRunAtNextTick :
        Maybe
            { current : Int
            , previous : Int
            }
    , animationDone : Evergreen.V8.Animator.Timeline Bool
    , importText : String
    , importReplaceNeedsConfirm : Bool
    , route : Evergreen.V8.Route.Route
    }


type alias BackendModel =
    { phrasebooks : Dict.Dict Evergreen.V8.Phrasebook.PhrasebookId Evergreen.V8.Phrasebook.Phrasebook
    , seed : Random.Seed
    }


type FrontendMsg
    = UrlClicked Browser.UrlRequest
    | UrlChanged Url.Url
    | ClickedCreatePhrasebook
    | AskedPhrase
    | GotPhrase Int
    | TypedPhrase Int String
    | ClickedAddPhrase
    | ClickedRemovePhrase Int
    | HitArrowKeyUpFrom Int
    | HitArrowKeyDownFrom Int
    | TypedImportText String
    | ClickedImportAppend
    | ClickedImportReplace
    | ConfirmedImportReplace
    | CancelledImportReplace
    | ClickedCopyId
    | ClickedCopyAddress
    | ClickedCopyPhrases
    | BlurredPhraseEditorInput Evergreen.V8.Phrase.PhraseId
    | FocusedPhraseEditorInput Evergreen.V8.Phrase.PhraseId
    | Tick Time.Posix
    | NoOp


type ToBackend
    = LoadPhrasebook Evergreen.V8.Phrasebook.PhrasebookId
    | SavePhrasebook Evergreen.V8.Phrasebook.PhrasebookId Evergreen.V8.Phrasebook.Phrasebook
    | CreatePhrasebook


type BackendMsg
    = NoOpBackendMsg


type ToFrontend
    = GotPhrasebook Evergreen.V8.Phrasebook.PhrasebookId Evergreen.V8.Phrasebook.Phrasebook
    | GotNewPhrasebookId Evergreen.V8.Phrasebook.PhrasebookId
    | SavedPhrasebook
