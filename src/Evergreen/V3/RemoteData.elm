module Evergreen.V3.RemoteData exposing (..)


type RemoteData error data
    = Initial
    | Loading
    | Loaded data
    | Errored error
