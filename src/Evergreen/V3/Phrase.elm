module Evergreen.V3.Phrase exposing (..)


type alias Phrase =
    ( Int, String )
