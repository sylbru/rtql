module Evergreen.V3.Types exposing (..)

import Browser
import Browser.Navigation
import Dict
import Evergreen.V3.Phrasebook
import Evergreen.V3.RemoteData
import Evergreen.V3.Route
import Http
import Random
import Url


type alias FrontendModel =
    { key : Browser.Navigation.Key
    , phrasebook : Maybe ( Evergreen.V3.Phrasebook.PhrasebookId, Evergreen.V3.RemoteData.RemoteData Http.Error Evergreen.V3.Phrasebook.Phrasebook )
    , currentPhrase : Int
    , importText : String
    , route : Evergreen.V3.Route.Route
    }


type alias BackendModel =
    { phrasebooks : Dict.Dict Evergreen.V3.Phrasebook.PhrasebookId Evergreen.V3.Phrasebook.Phrasebook
    , seed : Random.Seed
    }


type FrontendMsg
    = UrlClicked Browser.UrlRequest
    | UrlChanged Url.Url
    | ClickedCreatePhrasebook
    | AskedPhrase
    | GotPhrase Int
    | TypedPhrase Int String
    | ClickedAddPhrase
    | ClickedRemovePhrase Int
    | HitArrowKeyTowardsPhrase Int
    | TypedImportText String
    | ClickedImportReplace Evergreen.V3.Phrasebook.PhrasebookId (List String)
    | ClickedImportAppend Evergreen.V3.Phrasebook.PhrasebookId (List String)
    | ClickedCopy
    | NoOp


type ToBackend
    = LoadPhrasebook Evergreen.V3.Phrasebook.PhrasebookId
    | SavePhrasebook Evergreen.V3.Phrasebook.PhrasebookId Evergreen.V3.Phrasebook.Phrasebook
    | CreatePhrasebook


type BackendMsg
    = NoOpBackendMsg


type ToFrontend
    = GotPhrasebook Evergreen.V3.Phrasebook.PhrasebookId Evergreen.V3.Phrasebook.Phrasebook
    | GotNewPhrasebookId Evergreen.V3.Phrasebook.PhrasebookId
