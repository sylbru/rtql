module Evergreen.V3.Phrasebook exposing (..)

import Evergreen.V3.Phrase


type alias PhrasebookId =
    String


type alias Phrasebook =
    List Evergreen.V3.Phrase.Phrase
