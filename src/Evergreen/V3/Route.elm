module Evergreen.V3.Route exposing (..)

import Evergreen.V3.Phrasebook


type Page
    = Practice
    | Editor
    | Share
    | Import


type Route
    = Landing
    | Phrasebook Evergreen.V3.Phrasebook.PhrasebookId Page
