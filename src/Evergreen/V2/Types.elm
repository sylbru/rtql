module Evergreen.V2.Types exposing (..)

import Browser
import Browser.Navigation
import Dict
import Evergreen.V2.Phrasebook
import Evergreen.V2.RemoteData
import Evergreen.V2.Route
import Http
import Url


type alias FrontendModel =
    { key : Browser.Navigation.Key
    , phrasebook : Maybe ( Evergreen.V2.Phrasebook.PhrasebookId, Evergreen.V2.RemoteData.RemoteData Http.Error Evergreen.V2.Phrasebook.Phrasebook )
    , currentPhrase : Int
    , importText : String
    , route : Evergreen.V2.Route.Route
    }


type alias BackendModel =
    Dict.Dict Evergreen.V2.Phrasebook.PhrasebookId Evergreen.V2.Phrasebook.Phrasebook


type FrontendMsg
    = UrlClicked Browser.UrlRequest
    | UrlChanged Url.Url
    | AskedPhrase
    | GotPhrase Int
    | TypedPhrase Int String
    | ClickedAddPhrase
    | ClickedRemovePhrase Int
    | HitArrowKeyTowardsPhrase Int
    | TypedImportText String
    | ClickedImportReplace Evergreen.V2.Phrasebook.PhrasebookId (List String)
    | ClickedImportAppend Evergreen.V2.Phrasebook.PhrasebookId (List String)
    | ClickedCopy
    | NoOp


type ToBackend
    = LoadPhrasebook Evergreen.V2.Phrasebook.PhrasebookId
    | SavePhrasebook Evergreen.V2.Phrasebook.PhrasebookId Evergreen.V2.Phrasebook.Phrasebook


type BackendMsg
    = NoOpBackendMsg


type ToFrontend
    = GotPhrasebook Evergreen.V2.Phrasebook.PhrasebookId Evergreen.V2.Phrasebook.Phrasebook
