module Evergreen.V2.Route exposing (..)

import Evergreen.V2.Phrasebook


type Page
    = Practice
    | Editor
    | Export
    | Import


type Route
    = Landing
    | Phrasebook Evergreen.V2.Phrasebook.PhrasebookId Page
