module Evergreen.V2.Phrasebook exposing (..)

import Evergreen.V2.Phrase


type alias PhrasebookId =
    Int


type alias Phrasebook =
    List Evergreen.V2.Phrase.Phrase
