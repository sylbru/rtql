module Evergreen.V2.Phrase exposing (..)


type alias Phrase =
    ( Int, String )
