module Evergreen.V5.Phrasebook exposing (..)

import Evergreen.V5.Phrase


type alias PhrasebookId =
    String


type alias Phrasebook =
    List ( Evergreen.V5.Phrase.PhraseId, Evergreen.V5.Phrase.Phrase )
