module Evergreen.V5.Route exposing (..)

import Evergreen.V5.Phrasebook


type Page
    = Practice
    | Editor
    | Share
    | Import


type Route
    = Landing
    | Phrasebook Evergreen.V5.Phrasebook.PhrasebookId Page
