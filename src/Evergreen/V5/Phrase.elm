module Evergreen.V5.Phrase exposing (..)


type alias PhraseId =
    Int


type alias Phrase =
    String
