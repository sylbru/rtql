module Evergreen.V5.Types exposing (..)

import Browser
import Browser.Navigation
import Dict
import Evergreen.V5.Phrasebook
import Evergreen.V5.RemoteData
import Evergreen.V5.Route
import Http
import Random
import Url


type alias FrontendModel =
    { key : Browser.Navigation.Key
    , phrasebook : Maybe ( Evergreen.V5.Phrasebook.PhrasebookId, Evergreen.V5.RemoteData.RemoteData Http.Error Evergreen.V5.Phrasebook.Phrasebook )
    , currentPhrase : Int
    , importText : String
    , route : Evergreen.V5.Route.Route
    }


type alias BackendModel =
    { phrasebooks : Dict.Dict Evergreen.V5.Phrasebook.PhrasebookId Evergreen.V5.Phrasebook.Phrasebook
    , seed : Random.Seed
    }


type FrontendMsg
    = UrlClicked Browser.UrlRequest
    | UrlChanged Url.Url
    | ClickedCreatePhrasebook
    | AskedPhrase
    | GotPhrase Int
    | TypedPhrase Int String
    | ClickedAddPhrase
    | ClickedRemovePhrase Int
    | HitArrowKeyUpFrom Int
    | HitArrowKeyDownFrom Int
    | TypedImportText String
    | ClickedImportReplace Evergreen.V5.Phrasebook.PhrasebookId String
    | ClickedImportAppend Evergreen.V5.Phrasebook.PhrasebookId String
    | ClickedCopyId
    | ClickedCopyAddress
    | ClickedCopyPhrases
    | BlurredPhraseEditorInput
    | NoOp


type ToBackend
    = LoadPhrasebook Evergreen.V5.Phrasebook.PhrasebookId
    | SavePhrasebook Evergreen.V5.Phrasebook.PhrasebookId Evergreen.V5.Phrasebook.Phrasebook
    | CreatePhrasebook


type BackendMsg
    = NoOpBackendMsg


type ToFrontend
    = GotPhrasebook Evergreen.V5.Phrasebook.PhrasebookId Evergreen.V5.Phrasebook.Phrasebook
    | GotNewPhrasebookId Evergreen.V5.Phrasebook.PhrasebookId
    | SavedPhrasebook
