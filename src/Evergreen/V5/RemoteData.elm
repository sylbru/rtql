module Evergreen.V5.RemoteData exposing (..)


type RemoteData error data
    = Initial
    | Loading
    | Loaded data
    | Errored error
