module Evergreen.V7.Animator exposing (..)

import Evergreen.V7.Internal.Timeline


type alias Timeline state =
    Evergreen.V7.Internal.Timeline.Timeline state
