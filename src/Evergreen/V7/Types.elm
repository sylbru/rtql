module Evergreen.V7.Types exposing (..)

import Browser
import Browser.Navigation
import Dict
import Evergreen.V7.Animator
import Evergreen.V7.Phrase
import Evergreen.V7.Phrasebook
import Evergreen.V7.RemoteData
import Evergreen.V7.Route
import Http
import Random
import Time
import Url


type alias FrontendModel =
    { key : Browser.Navigation.Key
    , phrasebook : Maybe ( Evergreen.V7.Phrasebook.PhrasebookId, Evergreen.V7.RemoteData.RemoteData Http.Error Evergreen.V7.Phrasebook.Phrasebook )
    , currentPhrase : Int
    , previousPhrase : Maybe Int
    , animationDone : Evergreen.V7.Animator.Timeline Bool
    , importText : String
    , importReplaceNeedsConfirm : Bool
    , route : Evergreen.V7.Route.Route
    }


type alias BackendModel =
    { phrasebooks : Dict.Dict Evergreen.V7.Phrasebook.PhrasebookId Evergreen.V7.Phrasebook.Phrasebook
    , seed : Random.Seed
    }


type FrontendMsg
    = UrlClicked Browser.UrlRequest
    | UrlChanged Url.Url
    | ClickedCreatePhrasebook
    | AskedPhrase
    | GotPhrase Int
    | TypedPhrase Int String
    | ClickedAddPhrase
    | ClickedRemovePhrase Int
    | HitArrowKeyUpFrom Int
    | HitArrowKeyDownFrom Int
    | TypedImportText String
    | ClickedImportAppend
    | ClickedImportReplace
    | ConfirmedImportReplace
    | CancelledImportReplace
    | ClickedCopyId
    | ClickedCopyAddress
    | ClickedCopyPhrases
    | BlurredPhraseEditorInput Evergreen.V7.Phrase.PhraseId
    | FocusedPhraseEditorInput Evergreen.V7.Phrase.PhraseId
    | Tick Time.Posix
    | NoOp


type ToBackend
    = LoadPhrasebook Evergreen.V7.Phrasebook.PhrasebookId
    | SavePhrasebook Evergreen.V7.Phrasebook.PhrasebookId Evergreen.V7.Phrasebook.Phrasebook
    | CreatePhrasebook


type BackendMsg
    = NoOpBackendMsg


type ToFrontend
    = GotPhrasebook Evergreen.V7.Phrasebook.PhrasebookId Evergreen.V7.Phrasebook.Phrasebook
    | GotNewPhrasebookId Evergreen.V7.Phrasebook.PhrasebookId
    | SavedPhrasebook
