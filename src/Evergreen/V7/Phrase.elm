module Evergreen.V7.Phrase exposing (..)


type alias PhraseId =
    Int


type alias Phrase =
    String
