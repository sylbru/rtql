module Evergreen.V7.Route exposing (..)

import Evergreen.V7.Phrasebook


type Page
    = Practice
    | Editor
    | Share
    | Import


type Route
    = Landing
    | Phrasebook Evergreen.V7.Phrasebook.PhrasebookId Page
