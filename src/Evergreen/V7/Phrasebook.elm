module Evergreen.V7.Phrasebook exposing (..)

import Evergreen.V7.Phrase


type alias PhrasebookId =
    String


type alias Phrasebook =
    List ( Evergreen.V7.Phrase.PhraseId, Evergreen.V7.Phrase.Phrase )
