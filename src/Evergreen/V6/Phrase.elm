module Evergreen.V6.Phrase exposing (..)


type alias PhraseId =
    Int


type alias Phrase =
    String
