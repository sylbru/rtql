module Evergreen.V6.Route exposing (..)

import Evergreen.V6.Phrasebook


type Page
    = Practice
    | Editor
    | Share
    | Import


type Route
    = Landing
    | Phrasebook Evergreen.V6.Phrasebook.PhrasebookId Page
