module Evergreen.V6.Phrasebook exposing (..)

import Evergreen.V6.Phrase


type alias PhrasebookId =
    String


type alias Phrasebook =
    List ( Evergreen.V6.Phrase.PhraseId, Evergreen.V6.Phrase.Phrase )
