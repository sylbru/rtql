module Evergreen.V6.Types exposing (..)

import Browser
import Browser.Navigation
import Dict
import Evergreen.V6.Phrase
import Evergreen.V6.Phrasebook
import Evergreen.V6.RemoteData
import Evergreen.V6.Route
import Http
import Random
import Url


type alias FrontendModel =
    { key : Browser.Navigation.Key
    , phrasebook : Maybe ( Evergreen.V6.Phrasebook.PhrasebookId, Evergreen.V6.RemoteData.RemoteData Http.Error Evergreen.V6.Phrasebook.Phrasebook )
    , currentPhrase : Int
    , importText : String
    , route : Evergreen.V6.Route.Route
    }


type alias BackendModel =
    { phrasebooks : Dict.Dict Evergreen.V6.Phrasebook.PhrasebookId Evergreen.V6.Phrasebook.Phrasebook
    , seed : Random.Seed
    }


type FrontendMsg
    = UrlClicked Browser.UrlRequest
    | UrlChanged Url.Url
    | ClickedCreatePhrasebook
    | AskedPhrase
    | GotPhrase Int
    | TypedPhrase Int String
    | ClickedAddPhrase
    | ClickedRemovePhrase Int
    | HitArrowKeyUpFrom Int
    | HitArrowKeyDownFrom Int
    | TypedImportText String
    | ClickedImportReplace Evergreen.V6.Phrasebook.PhrasebookId
    | ClickedImportAppend Evergreen.V6.Phrasebook.PhrasebookId
    | ClickedCopyId
    | ClickedCopyAddress
    | ClickedCopyPhrases
    | BlurredPhraseEditorInput Evergreen.V6.Phrase.PhraseId
    | FocusedPhraseEditorInput Evergreen.V6.Phrase.PhraseId
    | NoOp


type ToBackend
    = LoadPhrasebook Evergreen.V6.Phrasebook.PhrasebookId
    | SavePhrasebook Evergreen.V6.Phrasebook.PhrasebookId Evergreen.V6.Phrasebook.Phrasebook
    | CreatePhrasebook


type BackendMsg
    = NoOpBackendMsg


type ToFrontend
    = GotPhrasebook Evergreen.V6.Phrasebook.PhrasebookId Evergreen.V6.Phrasebook.Phrasebook
    | GotNewPhrasebookId Evergreen.V6.Phrasebook.PhrasebookId
    | SavedPhrasebook
