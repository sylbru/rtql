module RemoteData exposing (RemoteData(..), fromData, map, unwrapWithDefault)


type RemoteData error data
    = Initial
    | Loading
    | Loaded data
    | Errored error


map : (data -> output) -> RemoteData error data -> RemoteData error output
map fn remoteData =
    case remoteData of
        Loaded data ->
            Loaded (fn data)

        Initial ->
            Initial

        Loading ->
            Loading

        Errored error ->
            Errored error


unwrapWithDefault : data -> RemoteData error data -> data
unwrapWithDefault default remoteData =
    case remoteData of
        Loaded data ->
            data

        _ ->
            default


fromData : data -> RemoteData error data
fromData data =
    Loaded data
