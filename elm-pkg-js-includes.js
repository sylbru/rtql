const copyPort = require('./elm-pkg-js/copy.js')
const copyAddressPort = require('./elm-pkg-js/copyAddress.js')

exports.init = async function init(app) {
    copyPort.init(app);
    copyAddressPort.init(app);
}